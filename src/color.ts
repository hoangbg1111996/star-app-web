export const ColorDefault = {
  primary: "#F03333",
  background: "rgb(242, 242, 242)",
  card: "rgb(255, 255, 255)",
  text: "rgb(28, 28, 30)",
  border: "rgb(216, 216, 216)",
  notification: "rgb(255, 59, 48)",
  error: "rgb(255, 59, 48)",
  info: "#ffd700",
  orange: "#ef733d",
  white: "#ffffff",
  line: "#CCCCCC",

  awesome: "#1BB590",
  good: "#7CBC22",
  okay: "#4AA1D6",
  bad: "#F28C26",
  terrible: "#F40D29",
  green: "#07B88F",
  base0: "#B4B4B4",
  base1: "#666666",
  base2: "#707070",
  base3: "#F2F2F2",
  base4: "#C4C4C4",
  base5: "#4A4A4A",
  base6: "#4C4C4C44",
  base7: "#333333",
  base8: "#0066D2",
  base9: "#000000",
};
