import "./App.css";

import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";

import { Card, Col, Divider, Rate, Row, Space } from "antd";
import { ColorDefault } from "./color";
import Rating from "./icons/rating.png";
import Comment from "./icons/comment.png";
import Ic_Bad from "./icons/ic_bad.png";
import Ic_Okay from "./icons/ic_okay.png";
import Ic_Good from "./icons/ic_good.png";
import Ic_Awesome from "./icons/ic_awesome.png";
import { useState } from "react";

ChartJS.register(ArcElement, Tooltip, Legend);

export const data = {
  labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

function App() {
  //state
  const [rate, setRate] = useState<number>(2.6);
  const totalReview = 230;
  //render
  return (
    <div className="App">
      <Row gutter={16}>
        {/* rating report */}
        <Col span={12}>
          <Card style={{ backgroundColor: ColorDefault.white }} hoverable>
            <Row gutter={40}>
              <Col span={12}>
                <div className="body-1">Điểm số</div>
                <Row justify={"space-between"} className="mt-16">
                  <Col span={12}>
                    <div className="img-container">
                      <img src={Rating} alt="rating" className="img-size-30" />
                    </div>
                  </Col>
                  <Col span={12}>
                    <Space size={8} direction="vertical" align="end">
                      <Rate
                        count={5}
                        value={rate}
                        allowHalf
                        autoFocus={false}
                        disabled
                      />
                      <div className="title-1">{rate} / 5</div>
                      <div className="body-2">Điểm trung bình</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <div className="body-1">Số đánh giá</div>
                <Row justify={"space-between"} className="mt-16">
                  <Col span={12}>
                    <div className="img-container">
                      <img
                        src={Comment}
                        alt="comment"
                        className="img-size-40"
                      />
                    </div>
                  </Col>
                  <Col span={12}>
                    <Space size={8} direction="vertical" align="end">
                      <div className="title-1">{totalReview}</div>
                      <div className="body-2">Tổng số đánh giá</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={16} className="mt-24">
              <Col span={6}>
                <Row justify={"space-between"}>
                  <Col>
                    <div className="img-container">
                      <img
                        src={Ic_Awesome}
                        alt="Ic_Awesome"
                        className="img-size-30"
                      />
                    </div>
                  </Col>
                  <Col>
                    <Space size={0} direction="vertical" align="end">
                      <div className="title-1">{totalReview}%</div>
                      <div className="body-2">5 sao</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
              <Col span={6} className="border-left">
                <Row justify={"space-between"}>
                  <Col>
                    <div className="img-container">
                      <img
                        src={Ic_Okay}
                        alt="Ic_Okay"
                        className="img-size-30"
                      />
                    </div>
                  </Col>
                  <Col>
                    <Space size={0} direction="vertical" align="end">
                      <div className="title-1">{totalReview}%</div>
                      <div className="body-2">4 sao</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
              <Col span={6} className="border-left">
                <Row justify={"space-between"}>
                  <Col>
                    <div className="img-container">
                      <img
                        src={Ic_Good}
                        alt="Ic_Good"
                        className="img-size-30"
                      />
                    </div>
                  </Col>
                  <Col>
                    <Space size={0} direction="vertical" align="end">
                      <div className="title-1">{totalReview}%</div>
                      <div className="body-2">3 sao</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
              <Col span={6} className="border-left">
                <Row justify={"space-between"}>
                  <Col>
                    <div className="img-container">
                      <img
                        src={Ic_Awesome}
                        alt="Ic_Awesome"
                        className="img-size-30"
                      />
                    </div>
                  </Col>
                  <Col>
                    <Space size={0} direction="vertical" align="end">
                      <div className="title-1">{totalReview}%</div>
                      <div className="body-2">2 sao</div>
                    </Space>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Card>
        </Col>

        {/* chart pie */}
        <Col span={12}>
          <Card style={{ backgroundColor: ColorDefault.white }} hoverable>
            <Doughnut data={data} />
          </Card>
        </Col>
      </Row>
      {/* comment list */}
      <Card className="mt-16" hoverable></Card>
    </div>
  );
}

export default App;
